package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"time"
)

var (
	rootDir          *string
	filesUpdateWait  *int
	files            = make(map[string]*File)
	fileBuildChan    = make(chan *File)
	extensionToCheck = ".go"
)

type File struct {
	Path    string
	Dir     string
	Name    string
	ModTime time.Time
}

func main() {
	rootDir = flag.String("root", ".", "path of directory to recurse and build go files")
	filesUpdateWait = flag.Int("updateWait", 5, "time in seconds to wait until rechecking file mods")
	flag.Parse()

	os.Chdir(*rootDir)

	go updateFilesInDir()
	buildFilesOnMod()
}

func updateFilesInDir() {
	for {
		getFilesWithExt(extensionToCheck)
		time.Sleep(time.Duration(*filesUpdateWait) * time.Second)
	}
}

func buildFile(file *File) {
	cmd := exec.Command("go", "build", ".")
	cmd.Dir = file.Dir
	log.Printf("building: %s", file.Path)
	stdoutStderr, err := cmd.CombinedOutput()
	if err == nil {
		Color.LogSuccess("BUILD SUCCESS")
	} else {
		Color.LogError("BUILD FAILURE")
		Color.LogError(fmt.Sprintf("exit status: %v", err))
		if len(stdoutStderr) != 0 {
			Color.LogError(fmt.Sprintf("output: \n%s", stdoutStderr))
		}
	}
}

func buildFilesOnMod() {
	var file *File
	for {
		file = <-fileBuildChan
		time.Sleep(1 * time.Second)
		buildFile(file)
	}
}

func getFilesWithExt(ext string) {
	filepath.Walk(*rootDir, func(path string, f os.FileInfo, _ error) error {
		if !f.IsDir() {
			if filepath.Ext(path) == ext {

				// If file is already in our map
				if val, ok := files[path]; ok {

					// If the file found is newer than the one we've stored,
					// tell our builder to build the file
					if val.ModTime.Before(f.ModTime()) {
						files[path].ModTime = f.ModTime()
						fileBuildChan <- files[path]
					}

					// If file is not in our map
				} else {
					log.Printf("found new go file: %s", path)
					files[path] = &File{
						Path:    path,
						Dir:     filepath.Dir(path),
						Name:    f.Name(),
						ModTime: f.ModTime(),
					}
				}
			}
		}
		return nil
	})
}
