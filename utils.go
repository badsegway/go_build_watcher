package main

import (
	"fmt"
	"log"
)

type Colorer struct {
	Codes map[string]uint8
}

var Color = Colorer{
	Codes: map[string]uint8{
		// Standard Colors
		"Black":  0,
		"Red":    1,
		"Green":  2,
		"Yellow": 3,
		"Blue":   4,
		"Purple": 5,
		"Cyan":   6,
		"White":  7,
		"Grey":   8,
		// High-Intensity colors
		"LRed":    9,
		"LGreen":  10,
		"LYellow": 11,
		"LBlue":   12,
		"LPurple": 13,
		"LCyan":   14,
		"LWhite":  15,
	},
}

func (self *Colorer) set(color interface{}, preCmd string) string {
	var cmdStr string
	argType := fmt.Sprintf("%T", color)
	if argType == "string" {
		cmdStr = fmt.Sprintf("%s%dm", preCmd, self.Codes[color.(string)])
	} else if argType == "int" {
		cmdStr = fmt.Sprintf("%s%dm", preCmd, color.(int))
	}
	return cmdStr
}

func (self *Colorer) log(color interface{}, msg string) string {
	return fmt.Sprintf("%s%s%s",
		self.SetForeground(color), msg, self.End())
}
func (self *Colorer) SetForeground(color interface{}) string {
	return self.set(color, "\033[1;38;05;")
}
func (self *Colorer) SetBackground(color interface{}) string {
	return self.set(color, "\033[1;48;05;")
}
func (self *Colorer) End() string {
	return "\033[0m"
}
func (self *Colorer) LogColor(color interface{}, msg string) {
	log.Printf(self.log("Green", msg))
}
func (self *Colorer) LogSuccess(msg string) {
	log.Printf(self.log("Green", msg))
}
func (self *Colorer) LogError(msg string) {
	log.Printf(self.log("Red", msg))
}
func (self *Colorer) LogWarning(msg string) {
	log.Printf(self.log("Yellow", msg))
}
func (self *Colorer) LogInfo(msg string) {
	log.Printf(self.log("Cyan", msg))
}
func (self *Colorer) LogComment(msg string) {
	log.Printf(self.log("Blue", msg))
}
func (self *Colorer) Println(color interface{}, msg string) {
	fmt.Println(self.log(color, msg))
}
func (self *Colorer) Print(color interface{}, msg string) {
	fmt.Printf(self.log(color, msg))
}
