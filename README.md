# description
Stupid-simple go builder for local builds+testing.
There's surely a better tool than this; but for those too lazy to search ...

# deps
* gitlab.com/badsegway/clicolor

# wut ... ?
### To begin recursively auto-building 'package main' projects from a root go project folder:
```bash
go install -u gitlab.com/BadSegway/go_build_watcher
cd /path/to/go/project/directory
go_build_watcher
```